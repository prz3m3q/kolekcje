package pl.bottega.com.kolekcje;

import org.junit.jupiter.api.*;
import pl.com.bottega.kolekcje.Book;
import pl.com.bottega.kolekcje.BookCollections;
import pl.com.bottega.kolekcje.Genre;
import pl.com.bottega.kolekcje.Person;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class BookCollectionTest {

    @Test
    public void testFindByAuthor() {
        Collection<Book> bookCollection2 = BookCollections.findByAuthor(getBookCollection1(), getAuthor2());
        assertTrue(bookCollection2.equals(getBookCollection2()));
    }

    @Test
    public void testFindByTitle() {
        Collection<Book> bookCollection2 = BookCollections.findByTitle(getBookCollection1(), "ATitl");
        assertTrue(bookCollection2.equals(getBookCollection2()));

        bookCollection2 = BookCollections.findByTitle(getBookCollection1(), "Titl");
        assertFalse(bookCollection2.equals(getBookCollection2()));
    }

    @Test
    public void testFindByGenres() {
        Collection<Book> findByGenresCollection = BookCollections.findByGenres(getBookCollection1(), getGenresSet2());
        assertTrue(findByGenresCollection.equals(getBookCollection2()));
    }

    @Test
    public void testSortByTile() {
        Collection<Book> sortedBookCollection = BookCollections.sortByTitle(getBookCollection1());
        assertTrue(sortedBookCollection.equals(getSortedBookCollection()));
    }

    @Test
    public void testGenresMap() {
        Map<Genre, Collection<Book>> genresMap = BookCollections.genresMap(getBookCollection1());

        Collection<Book> bookCollection = new LinkedList<>();
        bookCollection.add(getBook1());
        bookCollection.add(getBook3());
        assertTrue(genresMap.get(Genre.DRAMA).equals(bookCollection));

        bookCollection = new LinkedList<>();
        bookCollection.add(getBook1());
        assertTrue(genresMap.get(Genre.SATIRE).equals(bookCollection));

        bookCollection = new LinkedList<>();
        bookCollection.add(getBook2());
        assertFalse(genresMap.get(Genre.SATIRE).equals(bookCollection));
    }

    @Test
    public void testAuthorsMap() {
        Map<Person, Collection<Book>> authorsMap = BookCollections.authorsMap(getBookCollection1());

        Collection<Book> bookCollection = new LinkedList<>();
        bookCollection.add(getBook1());
        assertTrue(authorsMap.get(getAuthor1()).equals(bookCollection));

        bookCollection = new LinkedList<>();
        bookCollection.add(getBook2());
        assertTrue(authorsMap.get(getAuthor2()).equals(bookCollection));

        bookCollection = new LinkedList<>();
        bookCollection.add(getBook2());
        assertFalse(authorsMap.get(getAuthor3()).equals(bookCollection));
    }

    @Test
    public void testAuthorsBookCountMap() {
        Map<Person, Integer> authorsBookCountMap = BookCollections.authorsBookCountMap(getBookCollection1());

        assertTrue(authorsBookCountMap.get(getAuthor1()).equals(1));
        assertTrue(authorsBookCountMap.get(getAuthor2()).equals(1));
        assertTrue(authorsBookCountMap.get(getAuthor3()).equals(1));
        assertFalse(authorsBookCountMap.get(getAuthor3()).equals(2));
    }

    @Test
    public void testBookCountAuthor() {
        BookCollections bookCollections = new BookCollections();
        assertEquals(1, bookCollections.booksCount(getBookCollection1(), getAuthor1()));
        assertEquals(1, bookCollections.booksCount(getBookCollection1(), getAuthor2()));
        assertEquals(1, bookCollections.booksCount(getBookCollection1(), getAuthor3()));
        assertFalse(bookCollections.booksCount(getBookCollection1(), getAuthor3()) == 2);
    }

    @Test
    public void testBookCountGenre() {
        BookCollections bookCollections = new BookCollections();
        assertEquals(1, bookCollections.booksCount(getBookCollection1(), Genre.SATIRE));
        assertEquals(2, bookCollections.booksCount(getBookCollection1(), Genre.DRAMA));
        assertEquals(1, bookCollections.booksCount(getBookCollection1(), Genre.TRAGEDY));
        assertEquals(0, bookCollections.booksCount(getBookCollection1(), Genre.MYTHOLOGY));
    }

    @Test
    public void testMostPopularGenre() {
        BookCollections bookCollections = new BookCollections();
        assertEquals(Genre.DRAMA, bookCollections.mostPopularGenre(getBookCollection1()));
        assertFalse(bookCollections.mostPopularGenre(getBookCollection1()).equals(Genre.SATIRE));
    }

    private Collection<Book> getBookCollection1() {
        Collection<Book> books = new LinkedList<>();
        books.add(getBook1());
        books.add(getBook2());
        books.add(getBook3());
        return books;
    }

    private Collection<Book> getSortedBookCollection() {
        Collection<Book> books = new LinkedList<>();
        books.add(getBook2());
        books.add(getBook1());
        books.add(getBook3());
        return books;
    }

    private Collection<Book> getBookCollection2() {
        Collection<Book> books = new LinkedList<>();
        books.add(getBook2());
        return books;
    }

    private Set<Genre> getGenresSet1() {
        Set<Genre> genres = new HashSet<>();
        genres.add(Genre.COMEDY);
        genres.add(Genre.SATIRE);
        genres.add(Genre.DRAMA);
        return genres;
    }

    private Set<Genre> getGenresSet2() {
        Set<Genre> genres = new HashSet<>();
        genres.add(Genre.TRAGEDY);
        genres.add(Genre.TRAGICOMEDY);
        genres.add(Genre.FICTION);
        return genres;
    }

    private Set<Genre> getGenresSet3() {
        Set<Genre> genres = new HashSet<>();
        genres.add(Genre.HORROR);
        genres.add(Genre.DRAMA);
        return genres;
    }

    private Person getAuthor1() {
        return new Person("Imię", "Nazwisko", 22);
    }

    private Person getAuthor2() {
        return new Person("GImię", "GNazwisko", 25);
    }

    private Person getAuthor3() {
        return new Person("AImię", "BNazwisko", 27);
    }

    private Book getBook1() {
        return new Book("Title 1", getAuthor1(), getGenresSet1());
    }

    private Book getBook2() {
        return new Book("ATitle", getAuthor2(), getGenresSet2());
    }

    private Book getBook3() {
        return new Book("ZTitle 2", getAuthor3(), getGenresSet3());
    }
}
