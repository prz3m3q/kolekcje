package pl.com.bottega.kolekcje;

import java.util.*;

public class BookCollections {

    // zwraca książki z kolekcji books które zostały napisane przez zadanego autora
    // NIE modyfikuje kolekcji books!
    public static Collection<Book> findByAuthor(Collection<Book> books, Person author) {
        Collection<Book> authorBooks = new LinkedList<>();
        for (Book book : books) {
            if (book.writtenByAuthor(author)) {
                authorBooks.add(book);
            }
        }
        return authorBooks;
    }

    // zwraca książki z kolekcji books których tytuł zaczyna się od zadanej frazy,
    // wielkość liter nie ma znaczenia
    // NIE modyfikuje kolekcji books!
    public static Collection<Book> findByTitle(Collection<Book> books, String phrase) {
        Collection<Book> pharseBooks = new LinkedList<>();
        for (Book book : books) {
            if (book.titleStartsWith(phrase)) {
                pharseBooks.add(book);
            }
        }
        return pharseBooks;
    }

    // zwraca książki z kolekcji books które należą do wszystkich zadanych gatunków
    // NIE modyfikuje kolekcji books!
    public static Collection<Book> findByGenres(Collection<Book> books, Set<Genre> genres) {
        Collection<Book> genresBooks = new LinkedList<>();
        for (Book book : books) {
            if (book.hasGenres(genres)) {
                genresBooks.add(book);
            }
        }
        return genresBooks;
    }

    // zwraca posortowaną rosnąco po tytule listę książek stworzoną z kolekcji books
    // NIE modyfikuje kolekcji books!
    public static List<Book> sortByTitle(Collection<Book> books) {
        List<Book> sortByTitle = new LinkedList<>(books);
        sortByTitle.sort(new Comparator<Book>() {
            @Override
            public int compare(Book b1, Book b2) {
                return b1.getTitle().compareTo(b2.getTitle());
            }
        });
        return sortByTitle;
    }

    // zwraca posortowaną rosnąco listę książek z kolekcji books po nazwisku, imieniu autora i
    // po tytule
    // NIE modyfikuje kolekcji books!
    public static List<Book> sortByAuthorAndTitle(Collection<Book> books) {
        List<Book> sortByAuthorAndTitle = new LinkedList<>(books);
        sortByAuthorAndTitle.sort(new Comparator<Book>() {
            @Override
            public int compare(Book b1, Book b2) {
                if (!b1.getAuthor().getNazwisko().equals(b2.getAuthor().getNazwisko())) {
                    return b1.getAuthor().getNazwisko().compareTo(b2.getAuthor().getNazwisko());
                }
                if (!b1.getAuthor().getImie().equals(b2.getAuthor().getImie())) {
                    return b1.getAuthor().getImie().compareTo(b2.getAuthor().getImie());
                }
                return b1.getTitle().compareTo(b2.getTitle());
            }
        });
        return sortByAuthorAndTitle;
    }

    //tworzy mapę książek należących do poszczególnych gatunków, jeśli ksiżąka należy
    //do wielu gatunków, powinna wiele razy występować na mapie
    public static Map<Genre, Collection<Book>> genresMap(Collection<Book> books) {
        Map<Genre, Collection<Book>> genresMap = new HashMap<>();
        Collection<Book> bookCollection;
        for (Book book : books) {
            for (Genre genre : book.getGenres()) {
                bookCollection = genresMap.get(genre);
                if (bookCollection == null) {
                    bookCollection = new LinkedList<>();
                    genresMap.put(genre, bookCollection);
                }
                bookCollection.add(book);
            }
        }
        return genresMap;
    }

    //tworzy mapę książek należących napisanych przez poszczególnych autorów
    public static Map<Person, Collection<Book>> authorsMap(Collection<Book> books) {
        Map<Person, Collection<Book>> authorsMap = new HashMap<>();
        Collection<Book> bookCollection;
        for (Book book : books) {
            bookCollection = authorsMap.get(book.getAuthor());
            if (bookCollection == null) {
                bookCollection = new LinkedList<>();
                authorsMap.put(book.getAuthor(), bookCollection);
            }
            bookCollection.add(book);
        }
        return authorsMap;
    }

    //tworzy mapę z ilością książek napisanych przez zadanego autora
    public static Map<Person, Integer> authorsBookCountMap(Collection<Book> books) {
        Map<Person, Integer> authorsBookCountMap = new HashMap<>();
        Integer amount;
        for (Book book : books) {
            amount = authorsBookCountMap.get(book.getAuthor());
            if (amount == null) {
                authorsBookCountMap.put(book.getAuthor(), 1);
            } else {
                authorsBookCountMap.put(book.getAuthor(), ++amount);
            }
        }
        return authorsBookCountMap;
    }

    // zwraca liczbę książek których autorem jest auhtor
    public int booksCount(Collection<Book> books, Person author) {
        Integer amount = 0;
        for (Book book : books) {
            if (book.writtenByAuthor(author)) {
                amount++;
            }
        }
        return amount;
    }

    // zwraca liczbę książek z danego gatunku
    public int booksCount(Collection<Book> books, Genre genre) {
        Integer amount = 0;
        for (Book book : books) {
            if (book.hasGenre(genre)) {
                amount++;
            }
        }
        return amount;
    }

    // zwraca autora który napisał najwięcej książek
    //co w przypadku kiedy jeden autor ma tyle samo książek?
    public Person bestAuthor(Collection<Book> books) {
        Map<Person, Integer> authorsBookCountMap = authorsBookCountMap(books);
        Person author = null;
        Integer amount = 0;
        for (Map.Entry<Person, Integer> entry : authorsBookCountMap.entrySet()) {
            if (amount < entry.getValue()) {
                author = entry.getKey();
                amount = entry.getValue();
            }
        }
        return author;
    }

    // zwraca gatunek który ma najwięcej książek
    //co w przypadku kiedy dwa gatunki mają po tyle samo książek?
    public Genre mostPopularGenre(Collection<Book> books) {
        Map<Genre, Collection<Book>> genresMap = genresMap(books);
        Genre genre = null;
        Integer amount = 0;
        for (Map.Entry<Genre, Collection<Book>> entry : genresMap.entrySet()) {
            if (amount < entry.getValue().size()) {
                genre = entry.getKey();
                amount = entry.getValue().size();
            }
        }
        return genre;
    }

}