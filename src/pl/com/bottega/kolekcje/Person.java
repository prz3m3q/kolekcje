package pl.com.bottega.kolekcje;

public class Person {
    private String imie;
    private String nazwisko;
    private Integer wiek;
    private Gender gender;

    public Person(String imie, String nazwisko, Integer wiek) {
        if (imie == null || nazwisko == null || wiek == null) {
            throw new IllegalArgumentException();
        }
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (!imie.equals(person.imie)) return false;
        if (!nazwisko.equals(person.nazwisko)) return false;
        if (!wiek.equals(person.wiek)) return false;
        return gender != null ? gender.equals(person.gender) : person.gender == null;
    }

    @Override
    public int hashCode() {
        int result = imie.hashCode();
        result = 31 * result + nazwisko.hashCode();
        result = 31 * result + wiek.hashCode();
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        return result;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public Integer getWiek() {
        return wiek;
    }

    public Gender getGender() {
        return gender;
    }

    @Override
    public String toString() {
        return "Person{" +
            "imie='" + imie + '\'' +
            ", nazwisko='" + nazwisko + '\'' +
            ", wiek=" + wiek +
            ", gender=" + gender +
            '}';
    }
}
